const express =require('express');
const bodyParser = require('body-parser');
var db= require('./db.js');
db.sequelize.sync();
const app = express();
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
const { Op } = require("sequelize");
app.listen(8080);

app.get('/osoblje', function(request, response) {
    db.osoblje.findAll().then(function(ljudi){
        response.send(ljudi);
    });
});
app.get('/:dan/:semestar/:kraj/:pocetak/:naziv/:predavac/:periodicna', function(request, response){
    var dan_param=parseInt(request.params['dan'],10);
   // console.log(dan_param);
    var sem=0;
    if(request.params['semestar']=='zimski') 
        sem=1;
    else 
        sem=2;
    var ime_prezime=request.params['predavac'].split(' ');
    //console.log(ime_prezime.length, ime_prezime[0]);
    //console.log('hehe',parseInt(request.params['dan'],10));
    var per;
    if(request.params['periodicna']=='true') per=1;
    else per=0;
    db.rezervacija.findAll({
        include: [{
            model: db.termin, 
            as: 'rezervacijaTermin', 
            where: {                
                redovni: per,
                dan: dan_param,
                semestar: sem,
                pocetak: request.params['pocetak'],
                kraj: request.params['kraj']
            }},
            {
            model: db.sala, 
            as: 'rezervacijaSala',
            where:{
                naziv: request.params['naziv']
            }}, 
            {
            model:db.osoblje,
            where:{
                ime: ime_prezime[0],
                prezime:ime_prezime[1]
            }
            }

    ]}).then(function(data){
        var podaci;
        console.log(data);
        if(data.length==0) podaci=data;
        else podaci=data[0].osoblje;
        response.send(podaci);
    });
});
app.get('/:datum/:kraj/:pocetak/:sala/:predavac', function(request, response){
    console.log('radi');
    var ime_prezime=request.params['predavac'].split(' ');
    db.rezervacija.findAll({
        include: [{
            model: db.termin, 
            as: 'rezervacijaTermin', 
            where: { 
                datum: request.params['datum'],
                pocetak: request.params['pocetak'],
                kraj: request.params['kraj']
            }},
            {
            model: db.sala, 
            as: 'rezervacijaSala',
            where:{
                naziv: request.params['sala']
            }}, 
            {
            model:db.osoblje,
            where:{
                ime: ime_prezime[0],
                prezime:ime_prezime[1]
            }
            }

    ]}).then(function(data){
        var podaci;
        console.log(data);
        if(data.length==0) podaci=data;
        else podaci=data[0].osoblje;
        response.send(podaci);
    });    
});
app.post('/:dan/:semestar/:kraj/:pocetak/:naziv/:predavac/:periodicna', function(request, response){
    var sem=0;
    if(request.params['semestar']=='zimski') 
        sem=1;
    else 
        sem=2;
    var ime_prezime=request.params['predavac'].split(' ');
    db.osoblje.findOne({where:{ime:ime_prezime[0], prezime:ime_prezime[1]}}).then(osoba=>{
        //console.log('osoba',osoba.id);
        var id_osobe=osoba.id;
        db.sala.findOne({where:{naziv:request.params['naziv']}}).then(sala=>{
            var id_sale;
            if(sala!=null){
                id_sale=sala.id;    
            }
            else{
                var nova_sala=db.sala.build({
                    id:Math.floor(Math.random() * 1000),
                    naziv:request.params['naziv'],
                    zaduzenaOsoba:1
                });
                nova_sala.save().catch(err => console.log('Failed to create user', err));
                //console.log('novi id', nova_sala.id);
                id_sale=nova_sala.id;
            }
            db.termin.findOne({where:{redovni:true, dan:request.params['dan'], semestar:sem, pocetak:request.params['pocetak'], kraj:request.params['kraj']}}).then(termin=>{
                var id_termina;
                
                if(termin==null){
                    //console.log('nema termina');
                    var novi_termin=db.termin.build({
                        id: Math.floor(Math.random() * 1000),
                        redovni:true, 
                        dan: request.params['dan'],
                        semestar:sem,
                        pocetak:request.params['pocetak'],
                        kraj: request.params['kraj']
                    });
                    novi_termin.save().catch(err => console.log('Failed to create user', err));
                    id_termina=novi_termin.id;
                }
                else{
                    var id_termina=termin.id;
                    //console.log('ovojetemrin', termin.id);                    
                }
                db.rezervacija.create({
                    termin:id_termina,
                    sala: id_sale,
                    osoba:id_osobe
                }).then(function(user){
                    console.log('success');
                }).catch(function(err){
                    console.log('11err');
                });
            });
        });
    });
});
app.post('/:datum/:kraj/:pocetak/:sala/:predavac', function(request, response){
    var ime_prezime=request.params['predavac'].split(' ');
    //console.log('hi');
    db.osoblje.findOne({where:{ime:ime_prezime[0], prezime:ime_prezime[1]}}).then(osoba=>{
        //console.log('osoba',osoba.id);
        var id_osobe=osoba.id;
        db.sala.findOne({where:{naziv:request.params['sala']}}).then(sala1=>{
            var id_sale;
            if(sala1!=null){
                id_sale=sala1.id;    
            }
            else{
                var nova_sala=db.sala.build({
                    id:Math.floor(Math.random() * 1000),
                    naziv:request.params['sala'],
                    zaduzenaOsoba:1
                });
                nova_sala.save().catch(err => console.log('Failed to create user', err));
               // console.log('novi id', nova_sala.id);
            }
            db.termin.findOne({where:{redovni:false, datum:request.params['datum'], pocetak:request.params['pocetak'], kraj:request.params['kraj']}}).then(termin=>{
                var id_termina;
                if(termin==null){
                    //console.log('nema termina');
                    var novi_termin=db.termin.build({
                        id: Math.floor(Math.random() * 1000),
                        redovni:false, 
                        datum: request.params['datum'],
                        pocetak:request.params['pocetak'],
                        kraj: request.params['kraj']
                    });
                    novi_termin.save().catch(err => console.log('Failed to create user', err));
                    id_termina=novi_termin.id;

                }
                else{
                    id_termina=termin.id;
                    //console.log('ovojetemrin', termin.id);                    
                }
                db.rezervacija.create({
                    termin:id_termina,
                    sala: id_sale,
                    osoba:id_osobe
                }).then(function(user){
                    console.log('success');
                }).catch(function(err){
                    console.log('11err');
                });
            });
        });
    });

});
app.get('/osoblje1', function(request, response){
    db.osoblje.findAll().then(async function(ljudi){
        //console.log('hello');
        var danas=new Date();
        var dan_check=danas.getDay();//vraca 0-6
        var dd=danas.getDate();
        var mm=danas.getMonth()+1;
        var yyy=danas.getFullYear();
        var dat;
        if(dd<10 && mm<10){
            dat='0'+dd+'.0'+mm+'.'+yyy;
        }
        else if(dd>10 && mm<10){
            dat=dd+'.0'+mm+'.'+yyy;
        }
        else if(dd<10 && mm>10){
            dat='0'+dd+'.'+mm+'.'+yyy;
        }
        else    
            dat=dd+'.'+mm+'.'+yyy;

        //console.log('dd',dat);
        var niz=[];
        //console.log('osoblje',ljudi.length);
        var objekt;
        //console.log(ljudi.id);
        //niz=ljudi;
        for(covjek in ljudi){
            var ime_prezime=ljudi[covjek].ime+' '+ljudi[covjek].prezime;
            //console.log('ime', ime_prezime);
            //console.log('broj',ljudi[covjek]);
            await db.rezervacija.findOne({
                include:[{
                    model: db.termin,
                    as: 'rezervacijaTermin',
                    where:{
                        [Op.or]: [
                            {datum: dat },
                            {dan: dan_check }
                        ]
                    }
                    }, 
                    {
                    model: db.sala,
                    as: 'rezervacijaSala'
                    }                    
                ], 
                where:{osoba:ljudi[covjek].id}}).then(async function(rezervacije){
                    if(rezervacije==null){
                        objekt={ime:ime_prezime, sala:'u kancelariji'};
                        niz.push(objekt);
                    }
                    else{
                        //console.log(typeof(rezervacije.rezervacijaTermin.pocetak));
                        var sad=new Date();
                        var sat_sad=sad.getHours();
                        var min_sad=sad.getMinutes();
                        var vrijeme_pocetak=rezervacije.rezervacijaTermin.pocetak;
                        var sat_pocetak=parseInt(vrijeme_pocetak.substring(0,2));
                        var mins_pocetak=parseInt(vrijeme_pocetak.substring(3,5));
                        var vrijeme_kraj=rezervacije.rezervacijaTermin.pocetak;
                        var sat_kraj=parseInt(vrijeme_kraj.substring(0,2));
                        var mins_kraj=parseInt(vrijeme_kraj.substring(3,5));
                        if(sat_sad<sat_pocetak ||(sat_sad>sat_pocetak &&sat_sad>sat_kraj) || (sat_sad==sat_pocetak && min_sad<mins_pocetak)||(sat_sad==sat_pocetak && min_sad>mins_pocetak && sat_sad==sat_kraj && min_sad>min_kraj)||(sat_sad>sat_pocetak&&sat_sad==sat_kraj && min_sad>min_kraj)){
                            //pisat kanc
                            objekt={ime:ime_prezime, sala:'u kancelariji'};
                            niz.push(objekt);
                        }
                        else{
                            //console.log('sat',sat_sad, 'min', min_sad);
                            objekt={ime:ime_prezime, sala: rezervacije.rezervacijaSala.naziv};
                            niz.push(objekt);
                            //console.log( 'osoba',ime_prezime, 'eheh',rezervacije.rezervacijaSala.naziv);
                        }
                    }
            });
            
        }
        response.send(niz);
    });
});