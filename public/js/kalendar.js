let Kalendar = (function(){
    let periodicna_pp;
    let vandredna_pp;    
    function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj){
       
        if(periodicna_pp==null || vandredna_pp==null){
            var datumi=document.getElementsByClassName("ispod");            
            for(var i=0; i<datumi.length; i++){
                datumi[i].classList.remove("zauzeti");
                datumi[i].classList.add("slobodni");
            }         
        }

        else{            
            var datumi=document.getElementsByClassName("ispod");            
            for(var i=0; i<datumi.length; i++){
                datumi[i].classList.remove("zauzeti");
                datumi[i].classList.add("slobodni");
            }
            var semestar_rezervacije;
            if((mjesec<12 && mjesec>8)||mjesec==0){
                semestar_rezervacije="zimski";
            }
            else{
                semestar_rezervacije="ljetni";
            }            
            var niz_rezervacija_periodicnih=[];
            var niz_rezervacija_vandrednih=[];
            var dani_u_sedmici=document.getElementsByClassName("calendar__day");              
            var datumi_za_popuniti=document.getElementsByClassName("ispod");
            var broj_praznih_prije=document.getElementsByClassName("prazno_prije");
            var mjesec_vandredne_rezervacije;
            var dan_vandredne_rezervacije;    
            var br=[];
            for(var i=0; i<broj_praznih_prije.length; i++) {
                if(broj_praznih_prije[i].style.display!="none")
                    br.push(broj_praznih_prije[i]);

            }
            console.log('hej',vandredna_pp.length);
            for(var i=0; i<vandredna_pp.length; i++){
                mjesec_vandredne_rezervacije=parseInt(vandredna_pp[i].datum.substring(3,5))-1;
                if(mjesec_vandredne_rezervacije==mjesec && vandredna_pp[i].pocetak==pocetak && vandredna_pp[i].kraj==kraj && sala==vandredna_pp[i].naziv){
                    niz_rezervacija_vandrednih.push(vandredna_pp[i]);
                }                
            }
            for(var i=0; i<niz_rezervacija_vandrednih.length; i++){
                dan_vandredne_rezervacije=parseInt(niz_rezervacija_vandrednih[i].datum.substring(0,2));
                datumi_za_popuniti[dan_vandredne_rezervacije-1].classList.remove("slobodni");
                datumi_za_popuniti[dan_vandredne_rezervacije-1].classList.add("zauzeti");          
            }
            for(var i=0; i<periodicna_pp.length; i++){
                if(periodicna_pp[i].semestar==semestar_rezervacije && periodicna_pp[i].pocetak==pocetak && periodicna_pp[i].kraj==kraj && sala==periodicna_pp[i].naziv)
                    niz_rezervacija_periodicnih.push(periodicna_pp[i]);
            }
            for(var i=0; i<niz_rezervacija_periodicnih.length; i++){                 
                var j=0;  
                if(br.length>=niz_rezervacija_periodicnih[i].dan+1){
                    var x=7-br.length;
                    j=x+niz_rezervacija_periodicnih[i].dan;
                }
                else if(br.length<niz_rezervacija_periodicnih[i].dan+1){                 
                    j=niz_rezervacija_periodicnih[i].dan-br.length;
                }              
                while(j<datumi_za_popuniti.length){
                    datumi_za_popuniti[j].classList.remove("slobodni");
                    datumi_za_popuniti[j].classList.add("zauzeti");
                    j=j+7;
                }
            }
        }     
    }
    function ucitajPodatkeImpl(periodicna, vandredna){
        var godina;
        var regex=/[0-9][0-9]:[0-9][0-9]/;
            for(var i; i<periodicna.length; i++){    
                if(periodicna[i].pocetak.match(regex) && periodicna[i].kraj.match(regex)){
                    periodicna_pp.push(periodicna[i]);
                }
            }
            for(var i; i<vandredna.length; i++){  
                if(vandredna[i].pocetak.match(regex) && vandredna[i].kraj.match(regex) &&vandredna[i].datum.substring(5,9)=="2019"){
                    vandredna_pp.push(periodicna[i]);
                }
            }
       
        periodicna_pp=periodicna;
        vandredna_pp=vandredna;
    }
    function iscrtajKalendarImpl(kalendarRef, mjesec){
        if(mjesec<0 || mjesec>11){
            console.log("mjesec ne valja");
        }
        var datumi=document.getElementsByClassName("calendar__number"); 
        var datum=document.getElementById("datumi");
        var prazni=document.getElementsByClassName("prazno_prije");
        var element = document.createElement("div"); 
        element.classList.add("calendar__number");
        element.classList.add("hidden");
        element.classList.add("prazno_prije");
        element.setAttribute("id","element");
        var p_prime = element.cloneNode(true);
        p_prime.setAttribute("id","prime");
        var element31= document.createElement("div");
        element31.classList.add("calendar__number");
        element31.classList.add("hidden");
        var newContent = document.createTextNode("31");  
        element31.appendChild(newContent);
        var ispod_s=document.createElement("div");        
        var newContent1 = document.createTextNode(" ");  
        ispod_s.appendChild(newContent1);
        ispod_s.classList.add("ispod", "slobodni");
        element31.appendChild(ispod_s);
        element31.setAttribute("id", "tridesetjedan");
        if(mjesec==10){ 
            if(document.getElementById("element")!=null || document.getElementById("prime")!=null){
                document.getElementById("element").style.display="none";
                document.getElementById("prime").style.display="none";
                document.getElementById("tridesetjedan").style.display="none";
            }
            for(var i=0; i<4; i++){
                prazni[i].style.display="block";
            }    
            if(document.getElementById("tridesetjedan")!=null){
                document.getElementById("tridesetjedan").style.display="none";
            }    
            document.getElementById("mjesec").children[0].innerHTML="Novembar"; 
        }
        else if(mjesec==11){
            datum.insertBefore(element, datum.children[11]);
            datum.insertBefore(p_prime, datum.children[11]);
            if(document.getElementById("tridesetjedan")==null){
                datum.appendChild(element31);
            }   
            else document.getElementById("tridesetjedan").style.display="inline";   
            document.getElementById("mjesec").children[0].innerHTML="Decembar";           
        }
        else if(mjesec==0){
            for(var i=0; i<3; i++){
                prazni[i].style.display="none";
            }
            if(document.getElementById("tridesetjedan")==null){
                datum.appendChild(element31);
            }
            else document.getElementById("tridesetjedan").style.display="inline";
            document.getElementById("mjesec").children[0].innerHTML="Januar";
            document.getElementById("devet").style.display="block";
            document.getElementById("tri").style.display="block";
        }
        else if(mjesec==1){   
            for(var i=0; i<4; i++){
                prazni[i].style.display="block";
            }
            if(document.getElementById("element")!=null) document.getElementById("element").style.display="none";    
            if(document.getElementById("prime")!=null) document.getElementById("prime").style.display="none";
            if(document.getElementById("tridesetjedan")!=null){
                document.getElementById("tridesetjedan").style.display="none";
            }
            document.getElementById("devet").style.display="none";
            document.getElementById("tri").style.display="none";
            document.getElementById("mjesec").children[0].innerHTML="Februar";
        }
        else if(mjesec==2){
            for(var i=0; i<4; i++){
                prazni[i].style.display="block";
            }                          
            if(document.getElementById("element")!=null) document.getElementById("element").style.display="none";
            if(document.getElementById("prime")!=null) document.getElementById("prime").style.display="none";
            if(document.getElementById("tridesetjedan")==null){
                datum.appendChild(element31);
            }
            
            else document.getElementById("tridesetjedan").style.display="inline";
            document.getElementById("devet").style.display="block";
            document.getElementById("tri").style.display="block";
            document.getElementById("mjesec").children[0].innerHTML="Mart";
        }
        else if(mjesec==3){
            for(var i=0; i<4; i++){
                prazni[i].style.display="none";
            }            
            if(document.getElementById("element")!=null) document.getElementById("element").style.display="none";
            if(document.getElementById("prime")!=null) document.getElementById("prime").style.display="none";
            if(document.getElementById("tridesetjedan")!=null){
                document.getElementById("tridesetjedan").style.display="none";
            }
            document.getElementById("mjesec").children[0].innerHTML="April";
        }
        else if(mjesec==4){
            for(var i=0; i<2; i++){
                prazni[i].style.display="none";
            }
            prazni[2].style.display="block";
            prazni[3].style.display="block";
            if(document.getElementById("element")!=null) document.getElementById("element").style.display="none";
            if(document.getElementById("prime")!=null) document.getElementById("prime").style.display="none";
            if(document.getElementById("tridesetjedan")==null){
                datum.appendChild(element31);
            }
            else document.getElementById("tridesetjedan").style.display="inline";
            document.getElementById("mjesec").children[0].innerHTML="Maj";
        }
        else if(mjesec==5){
            for(var i=0; i<4; i++){
                prazni[i].style.display="block";
            }
            if(document.getElementById("tridesetjedan")!=null){
                document.getElementById("tridesetjedan").style.display="none";
            }
            if(document.getElementById("element"==null)) datum.insertBefore(element, datum.children[11]);
            else document.getElementById("element").style.display="block";
            document.getElementById("mjesec").children[0].innerHTML="Juni";
        }
        else if(mjesec==6){
            for(var i=0; i<4; i++){
                prazni[i].style.display="none";
            }
            if(document.getElementById("element")!=null){
                document.getElementById("element").style.display="none";                
            }
            if(document.getElementById("prime")!=null){
                document.getElementById("prime").style.display="none";                
            }
            if(document.getElementById("tridesetjedan")==null){
                datum.appendChild(element31);
            }
            else document.getElementById("tridesetjedan").style.display="inline";
            document.getElementById("mjesec").children[0].innerHTML="Juli";
        }
        else if(mjesec==7){
            prazni[0].style.display="none";
            prazni[1].style.display="block";
            prazni[2].style.display="block";
            prazni[3].style.display="block";
            if(document.getElementById("tridesetjedan")==null){
                datum.appendChild(element31);
            }
            else document.getElementById("tridesetjedan").style.display="inline";
            document.getElementById("mjesec").children[0].innerHTML="August";
            if(document.getElementById("element")!=null){
                document.getElementById("element").style.display="none";                
            }
            if(document.getElementById("prime")!=null){
                document.getElementById("prime").style.display="none";                
            }
        }
        else if(mjesec==8){
            if(document.getElementById("tridesetjedan")!=null){
                document.getElementById("tridesetjedan").style.display="none";
            } 
            if(document.getElementById("element")==null) datum.insertBefore(element, datum.children[11]);
            else element.style.display="block";
            if(document.getElementById("prime")==null) datum.insertBefore(p_prime, datum.children[11]);
            else p_prime.style.display="block";
            for(var i=0; i<prazni.length; i++){
                prazni[i].style.display="block";
            }
            document.getElementById("mjesec").children[0].innerHTML="Septembar";
        }
        else if(mjesec==9){
            for(var i=0; i<3; i++){
                prazni[i].style.display="none";
            }
            if(document.getElementById("tridesetjedan")==null){
                datum.appendChild(element31);
            }
            else document.getElementById("tridesetjedan").style.display="inline";
            if(document.getElementById("element")!=null) document.getElementById("element").style.display="none";
            if(document.getElementById("prime")!=null) document.getElementById("prime").style.display="none";            
            document.getElementById("mjesec").children[0].innerHTML="Oktobar";
        }
    }
    return {
    obojiZauzeca: obojiZauzecaImpl,
    ucitajPodatke: ucitajPodatkeImpl,
    iscrtajKalendar: iscrtajKalendarImpl
    }
    }());