
var podaci_periodicni=[{
    dan: 2,
    semestar: "zimski",
    kraj: "11:11",
    pocetak: "10:10",
    naziv: "0-02",
    predavac:"profesor"
},
{
    dan: 1,
    semestar: "ljetni",
    kraj: "11:11",
    pocetak: "10:10",
    naziv: "0-02",
    predavac:"profesor"

},
{
    dan: 6,
    semestar: "ljetni",
    kraj: "11:11",
    pocetak: "10:10",
    naziv: "0-02",
    predavac:"profesor"

}];
var podaci_vandredni=[{
    datum: "01.01.1992",
    pocetak: "11:11",
    kraj: "11:11",
    naziv: "0-02",
    predavac: "profesor"
},
{
    datum: "01.02.1992",
    pocetak: "00:00",
    kraj: "11:11",
    naziv: "0-02",
    predavac: "profesor"
}];
/*window.Ucitaj= function Ucitaj(){
    Kalendar.ucitajPodatke(podaci_periodicni, podaci_vandredni);
}*/
window.Promjena= function Promjena(){
    var sala=document.getElementById("sale_odabir").value;
    var kraj=document.getElementById("kraj").value;
    var pocetak=document.getElementById("pocetak").value;
    var mjesec=document.getElementById("mjesec").children[0].innerHTML;
    var mjesec_br;
    if(mjesec=="Januar") mjesec_br=0;
    if(mjesec=="Februar") mjesec_br=1;
    if(mjesec=="Mart") mjesec_br=2;
    if(mjesec=="April") mjesec_br=3;
    if(mjesec=="Maj") mjesec_br=4;
    if(mjesec=="Juni") mjesec_br=5;
    if(mjesec=="Juli") mjesec_br=6;
    if(mjesec=="August") mjesec_br=7;
    if(mjesec=="Septembar") mjesec_br=8;
    if(mjesec=="Oktobar") mjesec_br=9;
    if(mjesec=="Novembar") mjesec_br=10;
    if(mjesec=="Decembar") mjesec_br=11;
    Kalendar.obojiZauzeca(document.getElementById("kalendar"), mjesec_br, sala, pocetak, kraj);
}
window.mjesecPromjena1=function mjesecPromjena1(){
    var mjesec=document.getElementById("mjesec").children[0].innerHTML;
    var datumi=document.getElementsByClassName("ispod");
            for(var i=0; i<datumi.length; i++){
                datumi[i].classList.remove("zauzeti");
                datumi[i].classList.add("slobodni");
            } 
   
    if(mjesec=="Januar") document.getElementById("prethodni").disabled = true;
    else if(mjesec=="Februar") {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 0);
        document.getElementById("prethodni").disabled = true;
    }
    else if(mjesec=="Mart") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 1);
    else if(mjesec=="April") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 2);
    else if(mjesec=="Maj") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 3);
    else if(mjesec=="Juni") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 4);
    else if(mjesec=="Juli") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 5);
    else if(mjesec=="August") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 6);
    else if(mjesec=="Septembar") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 7);
    else if(mjesec=="Oktobar") {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 8);
        
    }
    else if(mjesec=="Novembar") {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 9);
        
    }
    else if(mjesec=="Decembar"){
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
        document.getElementById("sljedeci").disabled = false;
    }
}
window.mjesecPromjena2=function mjesecPromjena2(){
    var mjesec=document.getElementById("mjesec").children[0].innerHTML;
    var datumi=document.getElementsByClassName("ispod");
            
            for(var i=0; i<datumi.length; i++){
                datumi[i].classList.remove("zauzeti");
                datumi[i].classList.add("slobodni");
            } 
    if(mjesec=="Januar") {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 1);   
        document.getElementById("prethodni").disabled = false;
    }
    else if(mjesec=="Februar") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 2);
    else if(mjesec=="Mart") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 3);
    else if(mjesec=="April") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 4);
    else if(mjesec=="Maj") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 5);
    else if(mjesec=="Juni") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 6);
    else if(mjesec=="Juli") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 7);
    else if(mjesec=="August") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 8);
    else if(mjesec=="Septembar") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 9);
    else if(mjesec=="Oktobar") Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 10);
    else if(mjesec=="Novembar") {       
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 11);
        document.getElementById("sljedeci").disabled = true;
    }
    else if(mjesec=="Decembar") document.getElementById("sljedeci").disabled = true;
    
}
window.funkcija=function funkcija(){
    mjesecPromjena1();
    Promjena();
}
window.funkcija2=function funkcija(){
    mjesecPromjena2();
    Promjena();
}