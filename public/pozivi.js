let Pozivi = (function(){    
    function otvoriRezervacijeImpl(){
    var req= new XMLHttpRequest();
    req.open('GET', 'http://localhost:8080/zauzeca');
    req.onload=function(){
        var data=JSON.parse(req.responseText);
        var v=data.vandredna;
        var r=data.periodicna;
        Kalendar.ucitajPodatke(r,v);
        };
        //console.log('ajax');
    req.send();
    }
    function rezervisiImpl(){
        //console.log('thio',this.otvoriRezervacijeImpl()==null);
        var datumi_p=document.getElementsByClassName('calendar__number');
        var datumi=[];
        for( var i=0; i<datumi_p.length; i++){
            if(datumi_p[i].style.display!='none' && datumi_p[i].style.visibility!='hidden'&& !datumi_p[i].classList.contains('prazno_prije') && !datumi_p[i].childNodes[1].classList.contains('zauzeti')) 
                datumi.push(datumi_p[i]);
        }
        for(var i=0; i<datumi.length; i++){
            //console.log(datumi[i].childNodes[1].classList.contains('slobodni'));
            datumi[i].onclick=function(){
                var p;
                var v;
                var data;
                if(confirm('Da li želite da rezervišete ovaj termin?')){
                    var sala=document.getElementById('sale_odabir').value;
                    var periodicna=document.getElementsByName('periodicna')[0].checked;
                    var pocetak=document.getElementsByName('pocetak')[0].value;
                    var kraj=document.getElementsByName('kraj')[0].value;
                    var mjesec= document.getElementById('mjesec').childNodes[1].innerHTML;
                    var semestar;
                    if(mjesec=="Oktobar" || mjesec=="Novembar" || mjesec=="Decembar" || mjesec=="Januar")
                        semestar="zimski";
                    else if(mjesec=="Februar" || mjesec=="Mart" || mjesec=="April" || mjesec=="Maj" || mjesec=="Juni")
                        semestar="ljetni"; 
                    var br_mjeseca;
                    if(mjesec=="Janurar") br_mjeseca=1;
                    else if(mjesec=="Februar") br_mjeseca=2;
                    else if(mjesec=="Mart") br_mjeseca=3;
                    else if(mjesec=="April") br_mjeseca=4;
                    else if(mjesec=="Maj") br_mjeseca=5;
                    else if(mjesec=="Juni") br_mjeseca=6;
                    else if(mjesec=="Juli") br_mjeseca=7;
                    else if(mjesec=="August") br_mjeseca=8;
                    else if(mjesec=="Septembar") br_mjeseca=9;
                    else if(mjesec=="Oktobar") br_mjeseca=10;
                    else if(mjesec=="Novembar") br_mjeseca=11;
                    else if(mjesec=="Decembar") br_mjeseca=12;
                    var kliknuti=parseInt(this.innerHTML.substring(0,1));
                    //console.log('eh',kliknuti);
                    var x=kliknuti;
                    while(x>0){
                        if(x<7)break;
                        x=x-7;                         
                    }
                    var e = document.getElementById('select');
                    var predavac = e.options[e.selectedIndex].text;
                    //console.log('re', predavac);
                    //console.log( pocetak==0);
                    var datum_d;
                        var datum_m;
                        if(kliknuti<10){
                            datum_d='0'+kliknuti.toString(10);
                        }
                        else {
                            datum_d=kliknuti.toString(10);
                        }
                        if(br_mjeseca<10){
                            datum_m='0'+br_mjeseca.toString(10);
                        }
                        else {
                            datum_m=br_mjeseca.toString(10);
                        }
                        var datum=datum_d+'.'+ datum_m+'.2019';
                    if(pocetak==0){
                        alert('Unesite pocetak');
                    }
                    if(kraj==0){
                        alert('Unesite kraj');
                    }
                    if(periodicna){
                        var req= new XMLHttpRequest();
                        var url='http://localhost:8080/'+x+'/'+semestar+'/'+kraj+'/'+pocetak+'/'+sala+'/'+predavac;
                        //console.log(url);
                        req.open('GET', 'http://localhost:8080/'+x+'/'+semestar+'/'+kraj+'/'+pocetak+'/'+sala+'/'+predavac+'/'+periodicna);
                        req.onload=function(){
                            //console.log('ajax radi');
                            var data=JSON.parse(req.responseText);

                            //var data=JSON.parse(req.responseText);
                            console.log('ovo j data',data);

                            if(data==null || data.length==0){
                                //post      
                                var data11 = {"dan":kliknuti, "semestar":semestar, "kraj":kraj, "pocetak":pocetak, "naziv":sala, "predavac":predavac};
                                var xhr = new XMLHttpRequest();
                                xhr.withCredentials = true;
                                xhr.addEventListener("readystatechange", function () {
                                    if (this.readyState === 4) {
                                        console.log(this.responseText);
                                    }
                                    else{
                                        console.log('err');
                                    }
                                });
                                xhr.open('POST', 'http://localhost:8080/'+x+'/'+semestar+'/'+kraj+'/'+pocetak+'/'+sala+'/'+predavac+'/'+periodicna);
                                xhr.setRequestHeader("cache-control", "no-cache");
                                xhr.setRequestHeader("content-type", "application/json;charset=UTF-8");
                                //console.log('datdatdatdattda', data11);
                                //xhr.send(data11);
                                xhr.send(JSON.stringify(data11));
                                //Kalendar.obojiZauzeca(document.getElementById('kalendar'), mjesec-1, sala, pocetak, kraj);                          
                            }
                            else{
                                alert('Nije moguće rezervisati salu' + sala +'za navedeni datum' + kliknuti +'/' + br_mjeseca +'/2019 i termin od ' + pocetak +'do' + kraj + '!');
                            }
                        };
                        req.send();                        
                    }
                    else{
                        var req= new XMLHttpRequest();
                        
                        if(pocetak==0){
                            alert('Unesite pocetak');
                        }
                        if(kraj==0){
                            alert('Unesite kraj');
                        }
                        var url='http://localhost:8080/'+datum+'/'+kraj+'/'+pocetak+'/'+sala+'/'+predavac;
                        //console.log(url);
                        req.open('GET', 'http://localhost:8080/'+datum+'/'+kraj+'/'+pocetak+'/'+sala+'/'+predavac);
                        req.onload=function(){
                            //console.log('ajax radi');
                            var data=JSON.parse(req.responseText);
                            //console.log(data.length);
                            if(data.length==0){
                                //post
                                var data2 = {"dan":kliknuti, "semestar":semestar, "kraj":kraj, "pocetak":pocetak, "naziv":sala, "predavac":predavac};
                                var xhr = new XMLHttpRequest();
                                xhr.withCredentials = true;                                
                                xhr.addEventListener("readystatechange", function () {
                                    if (this.readyState === 4) {
                                        console.log(this.responseText);
                                    }
                                    else{
                                        console.log('err');
                                    }
                                });
                                //console.log('dta', data2);
                                xhr.open('POST', 'http://localhost:8080/'+datum+'/'+kraj+'/'+pocetak+'/'+sala+'/'+predavac);
                                //xhr.open('POST', 'http://localhost:8080/'+datum+'/'+kraj+'/'+pocetak+'/'+sala+'/'+predavac);
                                xhr.setRequestHeader("cache-control", "no-cache");
                                xhr.setRequestHeader("content-type", "application/json;charset=UTF-8");
                                xhr.send(JSON.stringify(data2));                             
                            }
                            else{
                                alert('Nije moguće rezervisati salu' + sala +'za navedeni datum' + kliknuti +'/' + br_mjeseca +'/2019 i termin od ' + pocetak +'do' + kraj + '!');
                            }
                        };
                        req.send();                        
                    }
                }              
            };
            
        }
    }    
    function pocetnaImpl(){
        var req= new XMLHttpRequest();
        req.open('GET', 'http://localhost:8080/files');
        req.onload=function(){
            //console.log('ajax');
            //console.log('res', req.responseText);
            var urls=JSON.parse(req.responseText);
            //console.log(urls.length, 'http://localhost:8080/slika/'+urls[1]);
            var req1=new XMLHttpRequest();
            req1.open('GET', 'http://localhost:8080/slika/'+urls[0]);
            var loadani=[];            
            req1.responseType = 'blob';
            req1.onload=function(){
                //console.log('slika');
                document.getElementById('slika1').src=URL.createObjectURL(this.response);
                loadani.push(this.response);
                document.getElementById('prethodni').disabled=true;
                var req2=new XMLHttpRequest();
                req2.open('GET', 'http://localhost:8080/slika/'+urls[1]);
                req2.responseType = 'blob';
                req2.onload=function(){
                    document.getElementById('slika2').src=URL.createObjectURL(this.response);
                    loadani.push(this.response);
                    var req3=new XMLHttpRequest();
                    req3.open('GET', 'http://localhost:8080/slika/'+urls[2]);
                    req3.responseType = 'blob';
                    req3.onload=function(){
                        var br=3;
                        document.getElementById('slika3').src=URL.createObjectURL(this.response);
                        loadani.push(this.response);
                        //console.log(br<urls.length);
                            document.getElementById('sljedeci').onclick=function(){
                                document.getElementById('prethodni').disabled=false;
                                var req4=new XMLHttpRequest();
                                req4.open('GET', 'http://localhost:8080/slika/'+urls[br]);
                                req4.responseType='blob';
                                req4.onload=function(){
                                    document.getElementById('slika1').src=URL.createObjectURL(this.response);
                                    loadani.push(this.response);
                                }
                                req4.send();
                                br++;
                                var req5=new XMLHttpRequest();
                                req5.open('GET', 'http://localhost:8080/slika/'+urls[br]);
                                req5.responseType='blob';
                                req5.onload=function(){
                                    document.getElementById('slika2').src=URL.createObjectURL(this.response);
                                    loadani.push(this.response);
                                }
                                req5.send();
                                br++;
                                var req6=new XMLHttpRequest();
                                req6.open('GET', 'http://localhost:8080/slika/'+urls[br]);
                                req6.responseType='blob';
                                req6.onload=function(){
                                    document.getElementById('slika3').src=URL.createObjectURL(this.response);
                                    loadani.push(this.response);
                                }
                                req6.send();
                                br++;
                                //console.log('ovojebrojac', br);
                                //console.log('looo', loadani);
                                if(urls.length-br<0){
                                    document.getElementById('sljedeci').disabled=true;
                                }
                                document.getElementById('prethodni').onclick=function(){
                                    //console.log('hi');                                    
                                    if(document.getElementById('sljedeci').disabled==true) 
                                        document.getElementById('sljedeci').disabled=false;
                                    document.getElementById('slika1').src='http://localhost:8080/slika/'+urls[br-6];
                                    //console.log(document.getElementById('slika1').src);
                                    document.getElementById('slika2').src='http://localhost:8080/slika/'+urls[br-5];
                                    document.getElementById('slika3').src='http://localhost:8080/slika/'+urls[br-4];
                                    if(br-6==0)
                                        document.getElementById('prethodni').disabled=true;
                                    br-=3;

                                }
                            }
                    } 
                    req3.send();
                }
                req2.send();

            }
            for(var i=0; i<urls.length; i+=3){
                //console.log(i);
            }
            req1.send();
        }
        req.send();
        
    }
    function selectImpl(){
        var req= new XMLHttpRequest();
        req.open('GET', 'http://localhost:8080/osoblje');
        req.onload=function(){
            var data=JSON.parse(req.responseText);
            //console.log(data.length);
            var index=0;
            var select= document.getElementById('select');
            //console.log('hh', data[0].ime);
            for(var i=0; i<data.length; i++){
               // console.log(data[i].ime);
                var opt = document.createElement("option");
                opt.value= index;
                opt.innerHTML = data[i].ime +' '+ data[i].prezime; 
                select.appendChild(opt);
                index++;
            }
        };
        //console.log('ajax');
    req.send();
    }
    function osobljeImpl(){
        var data_up;
        function ajaxreq(){
            //console.log(data_up, 'poce');
            var req= new XMLHttpRequest();
            req.open('GET', 'http://localhost:8080/osoblje1');
            req.onload=function(){
                //console.log('ajax radi');
                var isto=true;
                var data=JSON.parse(req.responseText); 
                if(data_up!=null){
                    if(data_up.length==data.length){
                        for(var i=0; i<data.length; i++){
                            if(JSON.stringify(data[i])!=JSON.stringify(data_up[i])){
                                isto=false;
                                break;
                            }
                        }
                    }   
                }          
                if(!isto || data_up==null){
                    var tableRef=document.getElementById('tabela').getElementsByTagName('tbody')[0];
                    var tab=document.getElementById('tabela');
                    if(!isto){
                        tableRef.innerHTML = tab.rows[0].innerHTML;
                    }
                   // console.log(data);
                    for(var i=0; i<data.length; i++){
                        var newRow=tableRef.insertRow();
                        var newCell=newRow.insertCell(0);
                        var newCell1=newRow.insertCell(1);
                        var newText=document.createTextNode(data[i].ime);
                        var newText1=document.createTextNode(data[i].sala);
                        newCell.appendChild(newText);  
                        newCell1.appendChild(newText1);
                    }
                    data_up=data;
                    //console.log('data',data);
                   // console.log('dataup', data_up==data);
                    //DA SE NE PONAVLJA! 
                }                                   
            };
           // console.log('ajax');
            req.send();
            return ajaxreq;
        }
        ajaxreq();
        setInterval(ajaxreq,30000);        
    }
    return {
    otvoriRezervacije: otvoriRezervacijeImpl,
    rezervisi: rezervisiImpl,
    pocetna: pocetnaImpl,
    select: selectImpl,
    osoblje: osobljeImpl
    }
}());
