let assert = chai.assert;
let expect = chai.expect;
function removeElement(id) {
  var elem = document.getElementById(id);
  return elem.parentNode.removeChild(elem);
}
function clear() {
  var datumi = document.getElementsByClassName('ispod'); 
  for(var i=0; i<datumi.length; i++){
    datumi[i].classList.remove("zauzeti");
    datumi[i].classList.add("slobodni");
} 
}
function clearKal(){
  var prazni=document.getElementsByClassName('prazno_prije');
  for(var i=0; i<prazni.length; i++){
    prazni[i].style.display='block';
  }
  var el1=document.getElementById('element');
  if(el1!=null) removeElement('element');
  var el2=document.getElementById('prime');
  if(el2!=null) removeElement('prime');
  var tri1=document.getElementById('tridesetjedan');
  if(tri1!=null) removeElement('tridesetjedan');

}
describe('Kalendar', function() {
 describe('obojiZauzeca()', function() {
   it('Pozivanje obojiZauzeca kada podaci nisu učitani: očekivana vrijednost da se ne oboji niti jedan dan', function() {    
    Kalendar.obojiZauzeca(document.getElementById('kalendar'), 1, '1-01', '11:11', '11.11');
    //console.log(document.getElementById('kalendar'))
    var ispod_niz = document.getElementsByClassName('ispod');  
    //console.log(ispod_niz);
      var dobro=true; 
      for(var i=0; i<ispod_niz.length; i++){
        //console.log(ispod_niz[i]);
          if(!ispod_niz[i].classList.contains('slobodni')) {
              dobro=false;
              break;
          }
      } 
      expect(dobro).to.be.true;
   });
   it('Pozivanje obojiZauzeca gdje u zauzecima postoje duple vrijednosti za zauzeće istog termina: očekivano je da se dan oboji bez obzira što postoje duple vrijednosti', function() {    
    var podaci_periodicni=[{      
        dan: 6,
        semestar: "zimski",
        kraj: "11:11",
        pocetak: "10:10",
        naziv: "0-02",
        predavac:"profesor"
    },
    {
        dan: 6,
        semestar: "zimski",
        kraj: "11:11",
        pocetak: "10:10",
        naziv: "0-02",
        predavac:"profesor"
    }  
    ];
    var prazan=[];
    Kalendar.ucitajPodatke(podaci_periodicni, prazan);
    var dobro=true; 
    Kalendar.obojiZauzeca(document.getElementById('kalendar'), 10 , '0-02', '10:10', '11:11');
    var ispod_niz = document.getElementsByClassName('ispod');  
    if(!ispod_niz[2].classList.contains('zauzeti')) dobro=false;
    //if(!ispod_niz[3].classList.contains('zauzeti')) dobro=false;
    expect(dobro).to.be.true;  
  });
  it('Pozivanje obojiZauzece kada u podacima postoji periodično zauzeće za drugi semestar: očekivano je da se ne oboji zauzeće', function() {    
    clear();
    var podaci_periodicni=[{
      dan: 0,
      semestar: "zimski",
      kraj: "11:11",
      pocetak: "11:11",
      naziv: "0-02",
      predavac:"profesor"
  },
  {
      dan: 1,
      semestar: "zimski",
      kraj: "11:11",
      pocetak: "11:11",
      naziv: "0-02",
      predavac:"profesor"
  
  }];
    var prazan=[];
    Kalendar.ucitajPodatke(podaci_periodicni, prazan);
    var dobro=true; 
    Kalendar.obojiZauzeca(document.getElementById('kalendar'), 3 , '0-02', '11:11', '11:11');
    var ispod_niz = document.getElementsByClassName('ispod');      
    for(var i=0; i<ispod_niz.length; i++){
        if(!ispod_niz[i].classList.contains('slobodni')) {
            dobro=false;
            //console.log(ispod_niz[i]);
            break;
        }
    }  
  expect(dobro).to.be.true;
  }); 
 it ('Pozivanje obojiZauzece kada u podacima postoji zauzeće termina ali u drugom mjesecu: očekivano je da se ne oboji zauzeće', function () {  
  var podaci_vandredni=[{
      datum: "01.11.1992",
      pocetak: "11:11",
      kraj: "11:11",
      naziv: "0-02",
      predavac: "profesor"
  }];
  clear();
  var prazan=[];
  Kalendar.ucitajPodatke(prazan, podaci_vandredni);
  var ispod_niz = document.getElementsByClassName('ispod');  
  //console.log(ispod_niz);
  var dobro=true; 
  Kalendar.obojiZauzeca(document.getElementById('kalendar'), 11 , '0-02', '10:10', '11:11');
  for(var i=0; i<ispod_niz.length; i++){
      if(!ispod_niz[i].classList.contains('slobodni')) {
          //console.log(ispod_niz[i].classList.contains('zauzeti'));
          dobro=false;
          break;
      }
  }  
  expect(dobro).to.be.true;      
});
it ('Pozivanje obojiZauzece kada su u podacima svi termini u mjesecu zauzeti: očekivano je da se svi dani oboje', function () {  
  var podaci_periodicni=[{
    dan: 0,
    semestar: "zimski",
    kraj: "11:11",
    pocetak: "11:11",
    naziv: "0-02",
    predavac:"profesor"
},
{
    dan: 1,
    semestar: "zimski",
    kraj: "11:11",
    pocetak: "11:11",
    naziv: "0-02",
    predavac:"profesor"

},
{
    dan: 2,
    semestar: "zimski",
    kraj: "11:11",
    pocetak: "11:11",
    naziv: "0-02",
    predavac:"profesor"

},
{
    dan: 3,
    semestar: "zimski",
    kraj: "11:11",
    pocetak: "11:11",
    naziv: "0-02",
    predavac:"profesor"

},
{
    dan: 4,
    semestar: "zimski",
    kraj: "11:11",
    pocetak: "11:11",
    naziv: "0-02",
    predavac:"profesor"

},
{
    dan: 5,
    semestar: "zimski",
    kraj: "11:11",
    pocetak: "11:11",
    naziv: "0-02",
    predavac:"profesor"

},
{
    dan: 6,
    semestar: "zimski",
    kraj: "11:11",
    pocetak: "11:11",
    naziv: "0-02",
    predavac:"profesor"

}];
  var prazan=[];
  clear();
  Kalendar.ucitajPodatke(podaci_periodicni,prazan);
  //console.log(ispod_niz);
  var dobro=true; 
   
  Kalendar.obojiZauzeca(document.getElementById('kalendar'), 11 , '0-02', '11:11', '11:11');
  var ispod_niz = document.getElementsByClassName('ispod'); 
  for(var i=0; i<ispod_niz.length; i++){
      if(!ispod_niz[i].classList.contains('zauzeti')) {
          //console.log(ispod_niz[i].classList.contains('zauzeti'));
          dobro=false;
          break;
      }
  }  
  expect(dobro).to.be.true;      
});
it ('Dva puta uzastopno pozivanje obojiZauzece: očekivano je da boja zauzeća ostane ista', function () {  
  var podaci_vandredni=[{
      datum: "01.11.1992",
      pocetak: "11:11",
      kraj: "11:11",
      naziv: "0-02",
      predavac: "profesor"
  }];
  clear();
  var prazan=[];
  Kalendar.ucitajPodatke(prazan, podaci_vandredni);
  Kalendar.obojiZauzeca(document.getElementById('kalendar'), 10, '0-02', '11:11', '11:11');
  var ispod_niz = document.getElementsByClassName('ispod');  
  //console.log(ispod_niz);
  Kalendar.obojiZauzeca(document.getElementById('kalendar'), 10, '0-02', '11:11', '11:11');
  var ispod_niz1 = document.getElementsByClassName('ispod');
  var dobro=true; 
  assert.deepEqual(ispod_niz,ispod_niz1);
});
it ('Pozivanje ucitajPodatke, obojiZauzeca, ucitajPodatke - drugi podaci, obojiZauzeca: očekivano da se zauzeća iz prvih podataka ne ostanu obojena, tj. primjenjuju se samo posljednje učitani podaci ', function () {  
  clear();
  var podaci_periodicni=[{
    dan: 4,
    semestar: "zimski",
    kraj: "11:11",
    pocetak: "11:11",
    naziv: "0-02",
    predavac:"profesor"
}];
var podaci_periodicni2=[{
    dan: 5,
    semestar: "zimski",
    kraj: "11:11",
    pocetak: "11:11",
    naziv: "0-02",
    predavac:"profesor"

}];
  var prazan=[];
  Kalendar.ucitajPodatke(podaci_periodicni, prazan);
  Kalendar.obojiZauzeca(document.getElementById('kalendar'), 10, '0-02', '11:11', '11:11');
  var ispod_niz = document.getElementsByClassName('ispod'); 
  console.log(ispod_niz);
  //clear(); 
  Kalendar.ucitajPodatke(podaci_periodicni2,prazan);
  Kalendar.obojiZauzeca(document.getElementById('kalendar'), 10, '0-02', '11:11', '11:11');
  var ispod_niz1 = document.getElementsByClassName('ispod');
  console.log(ispod_niz1);
  var dobro=true; 
  //console.log(ispod_niz[0].classList.contains('slobodni'));
  //console.log(ispod_niz1[1].classList.contains('zauzeti'));
  if(!ispod_niz[0].classList.contains('slobodni') || !ispod_niz1[1].classList.contains('zauzeti')) dobro=false;
 // console.log(ispod_niz1[0].classList.contains('zauzeti'));
 //popravit kod
  expect(dobro).to.be.true; 
});
it ('Provjera bojenja periodičnih rezervacija svakih 7 dana ', function () {  
  clear();
  var podaci_periodicni=[{
    dan: 4,
    semestar: "zimski",
    kraj: "11:11",
    pocetak: "11:11",
    naziv: "0-02",
    predavac:"profesor"
}];
  var prazan=[];
  Kalendar.ucitajPodatke(podaci_periodicni, prazan);
  Kalendar.obojiZauzeca(document.getElementById('kalendar'), 10, '0-02', '11:11', '11:11');
  var ispod_niz = document.getElementsByClassName('ispod'); 
  var dobro=true; 
  for(var i=0; i<ispod_niz.length; i+=7){
    if(!ispod_niz[i].classList.contains('zauzeti')) dobro=false;
  }
  expect(dobro).to.be.true; 
});
it ('Provjera bojenja vandrednih rezervacija za određeni dan ', function () {  
  clear();
  var podaci_vandredni=[{
    datum: "01.11.1992",
    pocetak: "11:11",
    kraj: "11:11",
    naziv: "0-02",
    predavac: "profesor"
}];
  var prazan=[];
  Kalendar.ucitajPodatke(prazan, podaci_vandredni);
  Kalendar.obojiZauzeca(document.getElementById('kalendar'), 10, '0-02', '11:11', '11:11');
  var ispod_niz = document.getElementsByClassName('ispod'); 
  var dobro=true; 
  if(!ispod_niz[0].classList.contains('zauzeti')) dobro=false;
  expect(dobro).to.be.true; 
});
});
describe('iscrtajKalendar()', function() {
  it ('Pozivanje iscrtajKalendar za mjesec sa 30 dana: očekivano je da se prikaže 30 dana', function () {  
    Kalendar.iscrtajKalendar(document.getElementById('kalendar'),10);
    var numbers=document.getElementsByClassName('calendar__number');
    var prazni=document.getElementsByClassName('prazno_prije');
    var dobro=true;
    //console.log(numbers);
    if(document.getElementById('tridesetjedan')!=null) dobro=false;
    if(numbers.length-prazni.length!=30) dobro=false;
    expect(dobro).to.be.true; 
});
it ('Pozivanje iscrtajKalendar za mjesec sa 31 dan: očekivano je da se prikaže 31 dan', function () {  
  clearKal();
  Kalendar.iscrtajKalendar(document.getElementById('kalendar'),11);
  var numbers=document.getElementsByClassName('calendar__number');
  var prazni=document.getElementsByClassName('prazno_prije');
  var dobro=true;
  console.log(numbers);
  if(document.getElementById('tridesetjedan')==null) dobro=false;
  if(numbers.length-prazni.length!=31) dobro=false;
  expect(dobro).to.be.true; 
});
it ('Pozivanje iscrtajKalendar za trenutni mjesec: očekivano je da je 1. dan u petak', function () {  
  clearKal();
  Kalendar.iscrtajKalendar(document.getElementById('kalendar'),10);
  //var numbers=document.getElementsByClassName('calendar__number');
  var prazni=document.getElementsByClassName('prazno_prije');
  var prazni_check=[];
  for(var i=0; i<prazni.length; i++){
    if(prazni[i].style.display!='none')
      prazni_check.push(prazni[i]);
  }
  var dobro=true;
  console.log(prazni_check.length);
  if(prazni_check.length!=4) dobro=false;
  expect(dobro).to.be.true; 
});
it ('Pozivanje iscrtajKalendar za trenutni mjesec: očekivano je da je 30. dan u subotu', function () {
  clearKal();  
  Kalendar.iscrtajKalendar(document.getElementById('kalendar'),10);
  var numbers=document.getElementsByClassName('calendar__number');
  var numbers_chack=[];
  for(var i=0; i<numbers.length; i++){
    if(numbers[i].style.display!='none')
      numbers_chack.push(numbers[i]);
  }
  var dobro=true;
  console.log(numbers_chack[numbers_chack.length-1].innerHTML.substring(0,2));
  if(numbers_chack[numbers_chack.length-1].innerHTML.substring(0,2)!=30) dobro=false; 
  console.log(dobro);
  if(numbers_chack[numbers_chack.length-29]!=numbers_chack[5]) dobro=false; 
  expect(dobro).to.be.true;   
});
it ('Pozivanje iscrtajKalendar za januar: očekivano je da brojevi dana idu od 1 do 31 počevši od utorka', function () {
  clearKal();  
  Kalendar.iscrtajKalendar(document.getElementById('kalendar'), 0);
  var numbers=document.getElementsByClassName('calendar__number');
  var prazni=document.getElementsByClassName('prazno_prije');
  var prazni_check=[];
  var numbers_chack=[];
  for(var i=0; i<prazni.length; i++){
    if(prazni[i].style.display!='none')
      //console.log('lalala', prazni[i]);
      prazni_check.push(numbers[i]);
  }
  for(var i=0; i<numbers.length; i++){

    if(numbers[i].style.display!='none')
      numbers_chack.push(numbers[i]);
  }
  
  console.log(prazni_check.length);
  assert.equal(prazni_check.length,1,'Prvi je u utorak ');
  for(var i=prazni_check.length; i<numbers_chack.length; i++){
    assert.equal(numbers_chack[i].innerHTML.substring(0,2),i,'OK');
  }
});
it ('Pozivanje iscrtajKalendar za oktobar: očekivano je da je 1. dan u utorak', function () {  
  clearKal();
  Kalendar.iscrtajKalendar(document.getElementById('kalendar'),9);
  //var numbers=document.getElementsByClassName('calendar__number');
  var prazni=document.getElementsByClassName('prazno_prije');
  var prazni_check=[];
  for(var i=0; i<prazni.length; i++){
    if(prazni[i].style.display!='none')
      prazni_check.push(prazni[i]);
  }  
  assert.equal(prazni_check.length, 1, 'ok'); 
});
it ('Pozivanje iscrtajKalendar za august: očekivano je da je 1. dan u cetvrtak', function () {  
  clearKal();
  Kalendar.iscrtajKalendar(document.getElementById('kalendar'),7);
  //var numbers=document.getElementsByClassName('calendar__number');
  var prazni=document.getElementsByClassName('prazno_prije');
  var prazni_check=[];
  for(var i=0; i<prazni.length; i++){
    if(prazni[i].style.display!='none')
      prazni_check.push(prazni[i]);
  }
  assert.equal(prazni_check.length, 3, 'ok'); 
});
});
});


