var expect  = require('chai').expect;
var request = require('request');
const superagent = require('superagent');
var app= require('../index');
process.env.NODE_ENV = 'test';
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
chai.use(chaiHttp);
const { Op } = require("sequelize");

var db= require('../db.js');

//IZBRISAT SVE STO NIJE OSNOVNO !!!!!! dodati termin id 3 ili naci termin koji nije 1 ili 2 jer ga napravi post 
describe('Osoblje', function(){
    it('Osoblje ucitavanje', function(done) {
        request('http://localhost:8080/osoblje' , function(error, response, body) {
            expect(response.statusCode).to.equal(200);
            done();
        });
    });
    it('Osoblje podaci oblik', function(done) {
        request('http://localhost:8080/osoblje' , function(error, response, body) {
            var osoblje=JSON.parse(response.body);
            expect(osoblje).to.be.an('array');
            expect(osoblje.length).to.equal(3);
            done();
        });
    });
    it('Osoblje podaci', function(done) {
        request('http://localhost:8080/osoblje' , function(error, response, body) {
            var osoblje=JSON.parse(response.body);
            expect(osoblje[0].ime).to.equal('Neko');
            expect(osoblje[0].prezime).to.equal('Nekic');
            expect(osoblje[1].ime).to.equal('Drugi');
            expect(osoblje[1].prezime).to.equal('Neko');
            expect(osoblje[2].ime).to.equal('Test');
            expect(osoblje[2].prezime).to.equal('Test');
            done();
        });
    });
});

describe('Sale', function () {
    it('Ucitava 2 sale', function (done) {        
      db.sala.findAll().then(function(sale){
        expect(sale).to.be.an('array');
      });
      done();
    });
    it('Ucitava 2 sale podaci', function (done) {        
        db.sala.findAll().then(function(sale){
            expect(sale[0].naziv).to.equal('1-11');
            expect(sale[1].naziv).to.equal('1-15');
            expect(sale[0].zaduzenaOsoba).to.equal(1);
            expect(sale[1].zaduzenaOsoba).to.equal(2);
        });
        done();
      });
});
describe('Rezervacija', function () {
    it('Ucitava 2 rezervacije', function (done) {  
      db.rezervacija.findAll().then(function(rezervacije){
        expect(rezervacije).to.be.an('array');
      });
      done();
    });
    it('Ucitava 2 rezervacija podaci', function (done) {        
        db.rezervacija.findAll().then(function(rezervacije){
            expect(rezervacije[0].termin).to.equal(1);
            expect(rezervacije[1].termin).to.equal(2);
            expect(rezervacije[0].sala).to.equal(1);
            expect(rezervacije[1].sala).to.equal(1);
            expect(rezervacije[0].osoba).to.equal(1);
            expect(rezervacije[1].osoba).to.equal(3);
        });
        done();
      });
      it('Azuriranje rezervacija', function(done){
          superagent
            .post('http://localhost:8080/2/zimski/12:00/11:00/1-11/Neko Nekic/true')
            .end(function(err,res){                
                if(err) done(err);
                db.termin.findOne({where:{dan:'2', kraj:'12:00', pocetak:'11:00'}}).then(function(taj){
                    var id_ter=taj.id;
                    db.rezervacija.findOne({where:{id:id_ter}}).then(function(r){
                        expect(taj).not.to.be.null;
                    })
                });                
            });
            done();
      })
});
describe('Kreiranje rezervacije', function () {
    it('Kreiranje', function (done) {
        db.termin.create(
            {
                id:3,
                redovni:true,
                dan:0,
                datum:null,
                semestar:2,
                pocetak:'13:00',
                kraj:'15:00'
            }
            ).then(function(user){
                console.log('success');
            }).catch(function(err){
                //console.log('err');
        });
        var rez=db.rezervacija.build({
            id: 7000,
            termin:3,
            sala:1,
            osoba:1
        });
        rez.save().catch(err => console.log('Failed to create user1'));;
        db.rezervacija.findOne({where:{termin:3,sala:1,osoba:1, id:7000}}).then(function(r){
            expect(r).to.not.be.null;
        });
      done();
    });
    it('Kreiranje dvije iste od iste osobe', function (done) {        
        var rez=db.rezervacija.build({
            id: Math.floor(Math.random() * 100),
            termin:3,
            sala:1,
            osoba:1
        });
        rez.save().catch(err => console.log('Failed to create user'));;
        db.rezervacija.findAll({where:{termin:3,sala:1,osoba:1}}).then(function(r){
            expect(r.length).to.equal(1);
        });
        done();
      });
      it('Kreiranje dvije iste od razlicite osobe', function(done){
        var rez=db.rezervacija.build({
            id: Math.floor(Math.random() * 100),
            termin:3,
            sala:1,
            osoba:2
        });
        rez.save().catch(err => console.log('Failed to create user'));;
        db.rezervacija.findAll({where:{termin:3,sala:1,osoba:2, id:rez.id}}).then(function(r){
            expect(r.length).to.equal(0);
        });
            done();
      })
});

