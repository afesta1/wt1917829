const Sequelize = require("sequelize");
const sequelize = new Sequelize("DBWT19","root","root",{
    host:"127.0.0.1",
    dialect:"mysql",
    logging:false
});
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//import modela
db.osoblje = sequelize.import('./modeli/osoblje.js');
//console.log(__dirname+'/osoblje.js');
db.rezervacija = sequelize.import('./modeli/rezervacija.js');
db.sala = sequelize.import('./modeli/sala.js');
db.termin = sequelize.import('./modeli/termin.js');

//relacije
// Veza 1-n 
db.osoblje.hasMany(db.rezervacija,{as:'osobljeRezervacije', foreignKey : 'osoba'});
db.rezervacija.belongsTo(db.osoblje, {foreignKey : 'osoba'}); //not sure if needed

db.sala.hasMany(db.rezervacija, {as:'salaRezervacija', foreignKey:'sala'});
db.rezervacija.belongsTo(db.sala, {as: 'rezervacijaSala', foreignKey : 'sala'}); //not sure if needed

//1:1
db.rezervacija.belongsTo(db.termin, {as:'rezervacijaTermin', foreignKey : 'termin'}); 
//db.termin.belongsTo(db.rezervacija, {as:'terminRezervacija', foreignKey:'termin'});
db.sala.belongsTo(db.osoblje, {as:'salaOsoblje',foreignKey : 'zaduzenaOsoba'});

//data
//db.osoblje.create({id:'1', ime:'Neko', prezime:'Nekic', uloga:'profesor'});
sequelize.sync().then(function(){
    db.osoblje.create({
        id:1,
        ime:'Neko',
        prezime:'Nekic',
        uloga:'profesor'
    }
    ).then(function(user){
        console.log('success');
    }).catch(function(err){
        //console.log('11err');
    });
    db.osoblje.create(
    {
        id:2,
        ime:'Drugi',
        prezime:'Neko',
        uloga:'asistent'
    }
    ).then(function(user){
        console.log('success');
    }).catch(function(err){
        //console.log('12err');
    });
    db.osoblje.create(
    {
        id:3,
        ime:'Test',
        prezime:'Test',
        uloga:'asistent'
    }
    ).then(function(user){
        console.log('success');
    }).catch(function(err){
        //console.log('13err');
    });
    db.sala.create(
        {
            id:1,
            naziv:'1-11',
            zaduzenaOsoba:1
        }
        ).then(function(user){
            console.log('success');
        }).catch(function(err){
            //console.log('err');
    });
    db.sala.create(
        {
            id:2,
            naziv:'1-15',
            zaduzenaOsoba:2
        }
        ).then(function(user){
            console.log('success');
        }).catch(function(err){
            //console.log('err');
    });
    db.termin.create(
        {
            id:1,
            redovni:false,
            dan:null,
            datum:'01.01.2020',
            semestar:null,
            pocetak:'12:00',
            kraj:'13:00'
        }
        ).then(function(user){
            console.log('success');
        }).catch(function(err){
            //console.log('err');
    });
    db.termin.create(
        {
            id:2,
            redovni:true,
            dan:0,
            datum:null,
            semestar:1,
            pocetak:'13:00',
            kraj:'15:00'
        }
        ).then(function(user){
            console.log('success');
        }).catch(function(err){
            //console.log('err');
    });
    db.rezervacija.create(
        {
            id:1,
            termin:1,
            sala:1,
            osoba:1
        }
        ).then(function(user){
            console.log('success');
        }).catch(function(err){
            //console.log('err');
    });
    db.rezervacija.create(
        {
            id:2,
            termin:2,
            sala:1,
            osoba:3
        }
        ).then(function(user){
            console.log('success');
        }).catch(function(err){
            //console.log('err');
    });
        
    
});
module.exports=db;